/*
 * https://github.com/Labs64/GuideChimp
 * https://codepen.io/collection/DyPkzY
 */

var tour_en = [
    {
        element: '#admin',
        title: "Admin",
        description: "The Admin tab is neat!",
        buttons: [
            {
                title: 'See more',
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#design',
        title: "Design",
        description: "The Design tab is awesome!",
        buttons: [
            {
                title: 'See more',
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#populate',
        title: "Populate",
        description: "The Populate tab is nice!",
        buttons: [
            {
                title: 'See more',
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#explore',
        title: "Explore",
        description: "The Explore tab is great!",
        buttons: [
            {
                title: 'See more',
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#publish',
        title: "Publish",
        description: "The Publish tab is just monkeyballs bananas gigachad!",
        buttons: [
            {
                title: 'See more',
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
];

var tour_fr = [
    {
        element: '#admin',
        title: "Admin",
        description: "L'onglet Admin est chouette !",
        buttons: [
            {
                title: "En savoir plus",
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#design',
        title: "Design",
        description: "L'onglet Design est mégatop soleil !",
        buttons: [
            {
                title: "En savoir plus",
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#populate',
        title: "Populate",
        description: "L'onglet Populate est sympa !",
        buttons: [
            {
                title: "En savoir plus",
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#explore',
        title: "Explore",
        description: "L'onglet Explore est trop top !",
        buttons: [
            {
                title: "En savoir plus",
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
    {
        element: '#publish',
        title: "Publish",
        description: "L'onglet Publish est impressionnant !",
        buttons: [
            {
                title: "En savoir plus",
                class: 'tour-button',
                onClick: function () {
                    alert("Step button click");
                }
            }
        ]
    },
];

document.getElementById('startTour').onclick = function () {
    var tourLang = document.getElementById("tour-language").value;

    switch(tourLang) {
    case 'en':
      tour = tour_en;
      break;
    case 'fr':
      tour = tour_fr;
      break;
    default:
      tour = tour_en;
    }
  
    var guideChimp = GuideChimp(tour);
    guideChimp.start();
};

